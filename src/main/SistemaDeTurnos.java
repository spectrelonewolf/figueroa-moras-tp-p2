package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SistemaDeTurnos {
	private String nombreSistema;
	private Map<Integer, Mesa> mesasDeVotacion;
	private Map<Integer, Turno> turnosDeVotacion;
	private Map<Integer, Persona> padronElectoral;

	/*
	 * Constructor del sistema de asignacion de turnos. Si el parametro nombre fuera
	 * null, debe generar una excepcion.
	 */
	public SistemaDeTurnos(String nombreSistema) {
		if (nombreSistema.equals(null)) {
			throw new RuntimeException("El nombre no puede ser null");
		} else {
			this.nombreSistema = nombreSistema;
			mesasDeVotacion = new HashMap<>();
			turnosDeVotacion = new HashMap<>();
			padronElectoral = new HashMap<>();
		}
	}

	/*
	 * Registrar a los votantes. Antes de asignar un turno el votante debe estar
	 * registrado en el sistema. Si no lo esta, se genera una excepcion. Si la edad
	 * es menor a 16, se genera una excepcion
	 */
	public void registrarVotante(int dni, String nombre, int edad, boolean enfPrevia, boolean trabaja) {
		if (edad < 16) {
			throw new RuntimeException("Es menos de edad");
		} else {
			if (padronElectoral.containsKey(dni)) {
				throw new RuntimeException("El votante ya esta registrado");
			} else {
				Persona p = new Persona(dni, nombre, esMayor(edad), enfPrevia, trabaja);
				padronElectoral.put(dni, p);
			}
		}
	}

	private boolean esMayor(int n) {
		return n > 65 ? true : false;
	}

	/*
	 * Agregar una nueva mesa del tipo dado en el parametro y asignar el presidente
	 * de cada una, el cual deberia estar entre los votantes registrados y sin turno
	 * asignado. - Devuelve el numero de mesa creada. si el president es un votante
	 * que no esta registrado debe devolver Empty si el tipo de mesa no es valido
	 * debe generar una excepcion Los tipos validos son: Enf_Preex, Mayor65, General
	 * y Trabajador
	 */
	public int agregarMesa(final String tipoMesa, int dni) {
		if (!padronElectoral.containsKey(dni)) {
			throw new RuntimeException("La persona no se encuentra en el registro de votantes");
		} else if (!nombreMesaCorrecto(tipoMesa)) {
			throw new RuntimeException("Tipo de Mesa incorrecto");
		} else {
			Persona presidente = padronElectoral.get(dni);
			int numeroDeMesa = agregarMesa(tipoMesa, presidente);
			return numeroDeMesa;
		}
	}

	private boolean nombreMesaCorrecto(String tipoMesa) {
		return tipoMesa == "Trabajador" || tipoMesa == "Enf_Preex" || tipoMesa == "Mayor65" || tipoMesa == "General";
	}

	private int agregarMesa(String tipoMesa, Persona presidente) {
		Mesa m = null;
		int numeroDeMesa = mesasDeVotacion.size() + 1;
		if (tipoMesa == "Trabajador") { 
			m = new MesaTrabajadores(numeroDeMesa, presidente);
		}
		if (tipoMesa == "Enf_Preex") {
			m = new MesaEnfermos(numeroDeMesa, presidente);
		}
		if (tipoMesa == "Mayor65") {
			m = new MesaMayores(numeroDeMesa, presidente);
		}
		if (tipoMesa == "General") {
			m = new MesaGeneral(numeroDeMesa, presidente);
		}
		mesasDeVotacion.put(numeroDeMesa, m);
		Turno turno = m.reservarTurno(presidente);
		turnosDeVotacion.put(presidente.getDni(), turno);
		return numeroDeMesa;
	}

	/*
	 * Asigna un turno a un votante determinado. - Si el dni no pertenece a un
	 * votante registrado debe generar una excepcion. - Si el votante ya tiene turno
	 * asignado se devuelve el turno como: Numero de Mesa y Franja Horaria. - Si aun
	 * no tiene turno asignado se busca una franja horaria disponible en una mesa
	 * del tipo correspondiente al votante y se devuelve el turno asignado, como
	 * Numero de Mesa y Franja Horaria. - Si no hay mesas con horarios disponibles
	 * no modifica nada y devuelve null. (Se supone que el turno permitira conocer
	 * la mesa y la franja horaria asignada)
	 */
	public Tupla<Integer, Integer> asignarTurno(int dni) {
		Tupla<Integer, Integer> tupla = null;
		Turno turno = null;
		Persona persona;
		if (!padronElectoral.containsKey(dni)) {
			throw new RuntimeException("La persona no se encuentra en el registro de votantes");
		} else {
			persona = padronElectoral.get(dni);
			if (!turnosDeVotacion.containsKey(dni)) {
				if (persona.isCertificadoDeTrabajo()) {
					turno = buscarTurno(persona, "Trabajador");
				} else if (persona.isEnfermedadPreexistente()) {
					turno = buscarTurno(persona, "Enf_Preex");
				} else if (persona.isMayorDe65()) {
					turno = buscarTurno(persona, "Mayor65");
				} else {
					turno = buscarTurno(persona, "General");
				}
			} else {
				return consultaTurno(dni);
			}
		}
		if (turno != null) {
			turnosDeVotacion.put(dni, turno);
			tupla = new Tupla<Integer, Integer>(turno.getNumeroMesa(), turno.getFranjaHoraria());
			return tupla;
		}
		return tupla;
	}

	private Turno buscarTurno(Persona persona, String tipo) {
		Turno turno = null;
		for (Mesa m : mesasDeVotacion.values()) {
			if (m instanceof MesaTrabajadores) {
				if (tipo == "Trabajador") {
					turno = m.reservarTurno(persona);
					return turno;
				}
			}
			else if (m instanceof MesaEnfermos) {
				if (tipo == "Enf_Preex") {
					turno = m.reservarTurno(persona);
					return turno;
				}
			}
			else if (m instanceof MesaMayores) {
				if (tipo == "Mayor65") {
					turno = m.reservarTurno(persona);
					return turno;
				}
			} 
			else if (m instanceof MesaGeneral){
				if (tipo == "General") {
					turno = m.reservarTurno(persona);
					return turno;
				}
			}
		}
		return turno;
	}

	/*
	 * Asigna turnos automoticamente a los votantes sin turno. El sistema busca si
	 * hay alguna mesa y franja horaria factible en la que haya disponibilidad.
	 * Devuelve la cantidad de turnos que pudo asignar.
	 */
	public int asignarTurnos() {
		int cantTurnos = 0;
		Turno turno = null;
		Persona persona = null;
		for (Integer dni : padronElectoral.keySet()) { 
			if (!turnosDeVotacion.containsKey(dni)) { 
				persona = padronElectoral.get(dni); 
				if (persona.isCertificadoDeTrabajo()) { 
					turno = buscarTurno(persona, "Trabajador");
				} else if (persona.isEnfermedadPreexistente()) {
					turno = buscarTurno(persona, "Enf_Preex");
				} else if (persona.isMayorDe65()) {
					turno = buscarTurno(persona, "Mayor65");
				} else {
					turno = buscarTurno(persona, "General");
				}
				if(turno!=null) {
					turnosDeVotacion.put(dni, turno);
					cantTurnos++;
				}
			}
		}
		return cantTurnos;
	}

	/*
	 * Hace efectivo el voto del votante determinado por su dni. Si el DNI no esta
	 * registrado entre los votantes debe generar una excepcion Si ya habia votado
	 * devuelve false. Sino, efectua el voto y devuelve true.
	 */
	public boolean votar(int dni) {
		if (!padronElectoral.containsKey(dni)) {
			throw new RuntimeException("La persona no fue registrada");
		}
		Persona p = padronElectoral.get(dni);
		if (p.isSePresentoAVotar()) {
			return false;
		}
		p.setSePresentoAVotar(true);
		return true;
	}

	/*
	 * Cantidad de votantes con Turno asignados al tipo de mesa que se pide. -
	 * Permite conocer cuantos votantes se asignaron hasta el momento a alguno de
	 * los tipos de mesa que componen el sistema de votacion. - Si la clase de mesa
	 * solicitada no es valida debe generar una excepcion
	 */
	public int votantesConTurno(final String tipoMesa) {
		int turnosAsignados = 0;
		if (!nombreMesaCorrecto(tipoMesa)) {
			throw new RuntimeException("La mesa ingresa es incorrecta");
		}
		for (Mesa m : mesasDeVotacion.values()) {
			if (tipoMesa == "trabajador" && m instanceof MesaTrabajadores) {
				turnosAsignados = turnosAsignados + cantidadTurnosMesa(m);
			}
			if (tipoMesa == "Enf_Preex" && m instanceof MesaEnfermos) {
				turnosAsignados = turnosAsignados + cantidadTurnosMesa(m);
			}
			if (tipoMesa == "Mayor65" && m instanceof MesaMayores) {
				turnosAsignados = turnosAsignados + cantidadTurnosMesa(m);
			}
			if (tipoMesa == "General" && m instanceof MesaGeneral) {
				turnosAsignados = turnosAsignados + cantidadTurnosMesa(m);
			}
		}
		return turnosAsignados;
	} 
	
	private int cantidadTurnosMesa(Mesa m) {
		int totalTurnos = 0;
		for (List<Turno> turnos : m.franjaHoraria.values()) {
			totalTurnos = totalTurnos + turnos.size();
		}
		return totalTurnos;
	}

	/*
	 * Consulta el turno de un votante dado su DNI. Devuelve Mesa y franja horaria.
	 * - Si el DNI no pertenece a un votante genera una excepcion. - Si el votante
	 * no tiene turno devuelve Empty.
	 */
	public Tupla<Integer, Integer> consultaTurno(int dni) {
		if (!padronElectoral.containsKey(dni)) {
			throw new RuntimeException("La persona no se encuentra en el registro de votantes");
		} else if (turnosDeVotacion.containsKey(dni)) {
			Tupla<Integer, Integer> tupla = obtenerTurno(dni);
			return tupla;
		} else {
			return null;
		}
	}

	private Tupla<Integer, Integer> obtenerTurno(int dni) {
		Turno turno = turnosDeVotacion.get(dni);
		Tupla<Integer, Integer> tupla = new Tupla<>(turno.getNumeroMesa(), turno.getFranjaHoraria());
		return tupla;
	}

	/*
	 * Dado un numero de mesa, devuelve una Map cuya clave es la franja horaria y el
	 * valor es una lista con los DNI de los votantes asignados a esa franja. Sin
	 * importar si se presentaron o no a votar. - Si el numero de mesa no es valido
	 * genera una excepcion. - Si no hay asignados devuelve un Map vacio.
	 */
	public Map<Integer, List<Integer>> asignadosAMesa(int numMesa) {
		if (!mesasDeVotacion.containsKey(numMesa)) {
			throw new RuntimeException("Numero de mesa incorrecto");
		}
		Map<Integer, List<Integer>> mesaCompleta = new HashMap<Integer, List<Integer>>();
		Mesa mesa = mesasDeVotacion.get(numMesa);
		mesaCompleta = mesa.dniVotantesDeMesa();
		return mesaCompleta;
	}

	/*
	 * Consultar la cantidad de votantes sin turno asignados a cada tipo de mesa.
	 * Devuelve una Lista de Tuplas donde se vincula el tipo de mesa con la cantidad
	 * de votantes sin turno que esperan ser asignados a ese tipo de mesa. La lista
	 * no puede tener 2 elementos para el mismo tipo de mesa.
	 */
	public List<Tupla<String, Integer>> sinTurnoSegunTipoMesa() {
		List<Tupla<String, Integer>> lista = new ArrayList<Tupla<String, Integer>>();
		Integer trabajador = 0;
		Integer enf_preex = 0;
		Integer mayor65 = 0;
		Integer general = 0;
		Persona persona;
		for (Integer n : padronElectoral.keySet()) {
			if (!turnosDeVotacion.containsKey(n)) {
				persona = padronElectoral.get(n);
				if (persona.isCertificadoDeTrabajo()) {
					trabajador++;
				} else if (persona.isEnfermedadPreexistente()) {
					enf_preex++;
				} else if (persona.isMayorDe65()) {
					mayor65++;
				} else {
					general++;
				}
			}
		}
		lista.add(new Tupla<>("Trabajador", trabajador));
		lista.add(new Tupla<>("Enf_Preex", enf_preex));
		lista.add(new Tupla<>("Mayor65", mayor65));
		lista.add(new Tupla<>("General", general));
		return lista;
	}
	
	@Override
	public String toString() {
		StringBuilder sB = new StringBuilder();
		sB.append("Sistema de turnos: ");
		sB.append(nombreSistema);
		sB.append("\n");
		sB.append(" mesasHabilitadas= ");
		sB.append("\n");
		sB.append(mesasDeVotacion.values().toString());
		sB.append("\n");
		sB.append(" TurnosEnEspera= ");
		sB.append("\n");
		sB.append(sinTurno());
		sB.append("\n");
		sB.append(" TurnosAsignados= ");
		sB.append("\n");
		sB.append(turnosDeVotacion.values().toString());
		sB.append("]");	
		return sB.toString();
	}
		
	private StringBuilder sinTurno () {
		StringBuilder sinTurno= new StringBuilder();
		for(Tupla <String , Integer > t: sinTurnoSegunTipoMesa()) {
			sinTurno.append(t.toString());
		}
		return sinTurno;
	}	

	

}
