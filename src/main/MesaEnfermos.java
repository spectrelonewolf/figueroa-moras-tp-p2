package main;

public class MesaEnfermos extends Mesa {

	public MesaEnfermos(int numeroMesa, Persona presidente) {
		super(numeroMesa,presidente,20,10);
	}

	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	@Override
	public String toString() {
			StringBuilder sB = new StringBuilder();
			sB.append("Mesa Enfermedades preexistentes [ ");
			sB.append(" Numero de mesa = ");
			sB.append(this.numeroDeMesa);
			sB.append(" | ");
			sB.append(" Presidente de mesa = ");
			sB.append(this.presidenteDeMesa.getNombre());
			sB.append(" ]");
			sB.append("\n");			
			return sB.toString();
	}
	
	
	
	
}
