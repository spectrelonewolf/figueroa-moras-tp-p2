package main;

public class Turno {
	private Integer dni;
	private Integer numeroDeMesa;
	private Integer franjaHoraria;

	public Turno(Integer dni, Integer numeroMesa, Integer franjaHoraria) {
		this.dni = dni;
		this.numeroDeMesa = numeroMesa;
		this.franjaHoraria = franjaHoraria;
	}
	
	public Integer getDni() {
		return dni;
	}

	public Integer getNumeroMesa() {
		return numeroDeMesa;
	}

	public Integer getFranjaHoraria() {
		return franjaHoraria;
	}

	@Override
	public String toString() {
			StringBuilder sB = new StringBuilder();
			sB.append("Turno [ ");
			sB.append(" DNI: ");
			sB.append(dni);
			sB.append(" Numero de mesa: ");
			sB.append(numeroDeMesa);
			sB.append(" Franja horaria: ");
			sB.append(franjaHoraria);
			sB.append(" ]");
			sB.append("\n");			
			return sB.toString();
	}

}
