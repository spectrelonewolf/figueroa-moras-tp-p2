package main;

public class Persona {

		private Integer dni;
		private String nombre;
		private boolean mayorDe65;
		private boolean enfermedadPreexistente;
		private boolean certificadoDeTrabajo;
		private boolean sePresentoAVotar;
		
		
		public Persona(Integer dni, String nombre, boolean mayorDe65, boolean enfermedadPreexistente,
				boolean certificadoDeTrabajo) {
			this.dni = dni;
			this.nombre = nombre;
			this.mayorDe65 = mayorDe65;
			this.enfermedadPreexistente = enfermedadPreexistente;
			this.certificadoDeTrabajo = certificadoDeTrabajo;
			this.sePresentoAVotar = false;
		}

		public Persona() {
		}

		public Integer getDni() {
			return dni;
		}

		public String getNombre() {
			return nombre;
		}


		public boolean isMayorDe65() {
			return mayorDe65;
		}
		
		public boolean isEnfermedadPreexistente() {
			return enfermedadPreexistente;
		}

		public boolean isCertificadoDeTrabajo() {
			return certificadoDeTrabajo;
		}

		public boolean isSePresentoAVotar() {
			return sePresentoAVotar;
		}

		public void setSePresentoAVotar(boolean sePresentoAVotar) {
			this.sePresentoAVotar = sePresentoAVotar;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Persona other = (Persona) obj;
			if (dni == null) {
				if (other.dni != null)
					return false;
			} else if (dni.equals(other.dni))
				return true;
			return true;
		}
		
		@Override
		public String toString() {
				StringBuilder sB = new StringBuilder();
				sB.append("Persona [ ");
				sB.append(" DNI: ");
				sB.append(dni);
				sB.append(" Nombre: ");
				sB.append(nombre);
				sB.append(" Mayor de 65: ");
				sB.append(mayorDe65);
				sB.append(" Enfermedad preexistente: ");
				sB.append(enfermedadPreexistente);
				sB.append(" Certificado de trabajo: ");
				sB.append(certificadoDeTrabajo);
				sB.append(" Se presento a votar: ");
				sB.append(sePresentoAVotar);			
				sB.append(" ]");
				sB.append("\n");			
				return sB.toString();
		}
}
