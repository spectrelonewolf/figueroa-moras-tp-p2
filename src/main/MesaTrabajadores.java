package main;
import java.util.ArrayList;
import java.util.List;

public class MesaTrabajadores extends Mesa {
	
	public MesaTrabajadores(int numeroMesa, Persona presidente) {
		super(numeroMesa,presidente,null,8);
	}
	
	@Override
	public Turno reservarTurno(Persona persona) {
		if(franjaHoraria.size()!=0) {
			List<Turno> listaDeTurnos = franjaHoraria.get(franja);
			Turno turno = new Turno(persona.getDni(),numeroDeMesa,franja);
			listaDeTurnos.add(turno);
			franjaHoraria.put(franja, listaDeTurnos);
			return turno;
		}
		else {
			List<Turno> listaDeTurnos = new ArrayList<>();
			Turno turno = new Turno(persona.getDni(),numeroDeMesa,franja);
			listaDeTurnos.add(turno);
			franjaHoraria.put(franja, listaDeTurnos);
			return turno;
		}
		
	}

	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sB = new StringBuilder();
		sB.append("Mesa con Certificado de trabajo [ ");
		sB.append(" Numero de mesa = ");
		sB.append(this.numeroDeMesa);
		sB.append(" | ");
		sB.append(" Presidente de mesa = ");
		sB.append(this.presidenteDeMesa.getNombre());
		sB.append(" ]");
		sB.append("\n");			
		return sB.toString();
}
}
