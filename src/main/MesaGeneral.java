package main;

public class MesaGeneral extends Mesa{
	
	public MesaGeneral(int numeroMesa, Persona presidente) {
		super(numeroMesa,presidente,30,10);
	}

	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sB = new StringBuilder();
		sB.append("Mesa General [ ");
		sB.append(" Numero de mesa = ");
		sB.append(this.numeroDeMesa);
		sB.append(" | ");
		sB.append(" Presidente de mesa = ");
		sB.append(this.presidenteDeMesa.getNombre());
		sB.append(" ]");
		sB.append("\n");			
		return sB.toString();
	}
	
}