package main;

public class Tupla<X, Y> {
	final X x;
	final Y y;
	
	public Tupla(X x, Y y) {
		this.x = x;
		this.y = y;
	}
	
	public X getX() {
		return x;
	}
	
	public Y getY() {
		return y;
	}

	@Override
	public String toString() {
		StringBuilder sB = new StringBuilder();
		sB.append("[");
		sB.append(x);
		sB.append(",");
		sB.append(y);
		sB.append("]");
		return sB.toString();
	}
}
