package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class Mesa {

	protected Integer numeroDeMesa;
	protected Persona presidenteDeMesa;
	protected Integer cupo;
	protected Integer franja;
	protected Map<Integer, List<Turno>> franjaHoraria;

	public Mesa(Integer numeroDeMesa, Persona presidenteDeMesa, Integer cupo, Integer franja) {
		this.numeroDeMesa = numeroDeMesa;
		this.presidenteDeMesa = presidenteDeMesa;
		this.cupo = cupo;
		this.franja = franja;
		this.franjaHoraria = new HashMap<>();
	}

	public int getCupo() {
		return cupo;
	}

	public int getFranja() {
		return franja;
	}

	public Turno reservarTurno(Persona persona) {
		Turno turno = null;
		Integer cont = 8;
		if (!franjaHoraria.isEmpty()) {
			for (List<Turno> lista : franjaHoraria.values()) {
				if (lista.size() < this.getCupo()) {
					turno = darTurno(persona.getDni(), this.numeroDeMesa, cont);
					lista.add(turno);
					franjaHoraria.put(cont, lista);
					return turno;
				}
				cont++;
			}
			if (franjaHoraria.size() < this.getFranja()) {
				List<Turno> listaDeTurnos = new ArrayList<Turno>();
				turno = darTurno(persona.getDni(), this.numeroDeMesa, cont);
				listaDeTurnos.add(turno);
				franjaHoraria.put(franjaHoraria.size() + 1, listaDeTurnos); // deberia usar el contador?
				return turno; //
			}
			return turno;
		} else {
			List<Turno> listaDeTurnos = new ArrayList<Turno>();
			turno = darTurno(persona.getDni(), this.numeroDeMesa, cont);
			listaDeTurnos.add(turno);
			franjaHoraria.put(cont, listaDeTurnos);
			return turno;
		}
	}

	private Turno darTurno(Integer dni, Integer numeroMesa, Integer hora) {
		Turno turno = new Turno(dni, numeroMesa, hora);
		return turno;
	}

	public Map<Integer, List<Integer>> dniVotantesDeMesa() {
		Map<Integer, List<Integer>> mesaCompleta = new HashMap<>();
		for (Integer franjaMap : franjaHoraria.keySet()) {
			List<Turno> lista = franjaHoraria.get(franjaMap);
			Iterator<Turno> turnosDeLaFranja = lista.iterator();
			List<Integer> listaNueva = new ArrayList<Integer>();
			while (turnosDeLaFranja.hasNext()) {
				Turno turno = turnosDeLaFranja.next();
				listaNueva.add(turno.getDni());
			}
			mesaCompleta.put(franjaMap, listaNueva);
		}
		return mesaCompleta;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		Mesa other = (Mesa) obj;
		if (numeroDeMesa == null) {
			if (other.numeroDeMesa != null) {
				return false;
			}
		} else if (numeroDeMesa.equals(other.numeroDeMesa)) {
			return true;
		}
		if (presidenteDeMesa == null) {
			if (other.presidenteDeMesa != null) {
				return false;
			}
		} else if (presidenteDeMesa.equals(other.presidenteDeMesa)) {
			return true;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		if (this == obj) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public abstract String toString();

}
